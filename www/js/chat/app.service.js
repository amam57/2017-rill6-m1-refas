var app = angular.module('app');

app.service('countriesService',function($http,apiHost){
	this.getAllCountry = function (){
		/**
		* @ return Promise
		*/
		return $http.get(apiHost + '/countries');
	}
});



app.service('contratCountryService',function($http,apiHost){
	this.getContratById = function ($codePays){
		/**
		* @ return Promise
		**/
		// modif rajouter s
		return $http.get(apiHost + '/countries/'+ $codePays +'/contracts');
	}
	this.getNameCountry = function ($codePays){
		/**
		* @ return Promise
		**/
		return $http.get(apiHost + '/countries/'+ $codePays);
	}
});


app.service('stationContratService',function($http,apiHost){
	this.getStationByContratId = function ($idContrat){
		/**
		* @ return Promise
		**/
		return $http.get(apiHost + '/contracts/'+$idContrat+'/stations');
	}
});

app.service('calcGeolocStation',function(){
	this.calculateDistance = function(mypos,posStation){
		return (google.maps.geometry.spherical.computeDistanceBetween(mypos, posStation) / 1000).toFixed(2);
	};
})