var app = angular.module('app');

app.controller('countriesListController', function($scope,countriesService) {
	$scope.message = 'Hello World';
	countriesService.getAllCountry()
		.then(function (response){
			console.log(response.data);
			$scope.countries = response.data;
		})
		.catch(function (error){
			console.error(error.message);
	});
	$scope.getContrat = function(idContrat){
		console.log(idContrat);
	};
});

app.controller('countryDetailController', function($scope,$stateParams,contratCountryService) {
	contratCountryService.getContratById($stateParams.iso)
		.then(function (response){
			console.log(response.data);
			$scope.contrats = response.data;
		})
		.catch(function (error){
			console.error(error.message);
		});
	contratCountryService.getNameCountry($stateParams.iso)
		.then(function (response){
			console.log(response.data);
			$scope.nameCountry = response.data;
		})
		.catch(function (error){
			console.error(error.message);
		});

});


app.controller('stationContratController', function($scope,$stateParams,stationContratService,$interval,$cordovaGeolocation,calcGeolocStation) {
	var listStation = function (){
		stationContratService.getStationByContratId($stateParams.idcontrat)
			.then(function (response){
				var posOptions = {timeout: 10000, enableHighAccuracy: false};
				$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
					var pos = new google.maps.LatLng(position.coords.latitude,position.coords.longitude);
					response.data.forEach(function(item) {
						item.distance = calcGeolocStation.calculateDistance(pos,new google.maps.LatLng(item.latitude,item.longitude));
					});
					$scope.namestation = $stateParams.idcontrat;
					$scope.stations = response.data;
				}, function(err) {
					// error
					console.error(error.message);
				});
			})
			.catch(function (error){
				console.error(error.message);
			});
	}

	listStation();
	$interval(listStation,60000)
		.then(function(response){
		}, function (error){
			console.error("Error : " + error.message);
		}, function(notify){
			console.log('refresh OK');
		});
});



app.controller('mapsController', function(NgMap,$scope,$cordovaGeolocation,countriesService,contratCountryService) {
  NgMap.getMap().then(function(map) {
  	var posOptions = {timeout: 10000, enableHighAccuracy: false};
  	$scope.mesures= [];

	$cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
		countriesService.getAllCountry()
			.then(function (response){
				response.data.forEach(function(item){
					contratCountryService.getContratById(item.iso2)
						.then(function (response){
							response.data.forEach(function(value){
								$scope.mesures.push({
									longitude: value.longitude,
									latitude: value.latitude
								});
							})
						})
						.catch(function (error){
							console.error(error.message);
						});					
				});
			})
			.catch(function (error){
				console.error(error.message);
		});
		$scope.latitude = position.coords.latitude;
		$scope.longitude = position.coords.longitude;
	}, function(err) {
		// error
		console.error(error.message);
	});  	
  });
});
