var app = angular.module('app');

app.config(function($stateProvider, $urlRouterProvider){
	$urlRouterProvider.otherwise('/');
	$stateProvider.state('countriesList',{
		url: '/',
		controller : 'countriesListController',
		templateUrl : '/templates/countries/list.html'
	});
});

app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider.state('countryDetail',{
		url: '/country/:iso',
		controller : 'countryDetailController',
		templateUrl : '/templates/countries/details.html'
	});
});

app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider.state('stationContrat',{
		url: '/country/:countryIso/:idcontrat',
		controller : 'stationContratController',
		templateUrl : '/templates/station/list.html'
	});
});

app.config(function($stateProvider, $urlRouterProvider){
	$stateProvider.state('maps',{
		url: '/maps',
		controller : 'mapsController',
		templateUrl : '/templates/maps/index.html'
	});
});

